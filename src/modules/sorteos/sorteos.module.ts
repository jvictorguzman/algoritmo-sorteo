import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SorteosService } from './sorteos.service';
import { SorteosController } from './sorteos.controller';
import { Sorteo } from './entities/sorteo.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Sorteo])]
  controllers: [SorteosController],
  providers: [SorteosService]
})
export class SorteosModule { }
